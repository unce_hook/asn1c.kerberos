/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#include "S4UUserID.h"

asn_TYPE_member_t asn_MBR_S4UUserID_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct S4UUserID, nonce),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Krb5uint32,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"nonce"
		},
	{ ATF_POINTER, 1, offsetof(struct S4UUserID, cname),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_PrincipalName,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"cname"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct S4UUserID, crealm),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Realm,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"crealm"
		},
	{ ATF_POINTER, 2, offsetof(struct S4UUserID, subject_certificate),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_OCTET_STRING,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"subject-certificate"
		},
	{ ATF_POINTER, 1, offsetof(struct S4UUserID, options),
		(ASN_TAG_CLASS_CONTEXT | (4 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_BIT_STRING,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"options"
		},
};
static const int asn_MAP_S4UUserID_oms_1[] = { 1, 3, 4 };
static const ber_tlv_tag_t asn_DEF_S4UUserID_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_S4UUserID_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* nonce */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* cname */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* crealm */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 }, /* subject-certificate */
    { (ASN_TAG_CLASS_CONTEXT | (4 << 2)), 4, 0, 0 } /* options */
};
asn_SEQUENCE_specifics_t asn_SPC_S4UUserID_specs_1 = {
	sizeof(struct S4UUserID),
	offsetof(struct S4UUserID, _asn_ctx),
	asn_MAP_S4UUserID_tag2el_1,
	5,	/* Count of tags in the map */
	asn_MAP_S4UUserID_oms_1,	/* Optional members */
	3, 0,	/* Root/Additions */
	5,	/* First extension addition */
};
asn_TYPE_descriptor_t asn_DEF_S4UUserID = {
	"S4UUserID",
	"S4UUserID",
	&asn_OP_SEQUENCE,
	asn_DEF_S4UUserID_tags_1,
	sizeof(asn_DEF_S4UUserID_tags_1)
		/sizeof(asn_DEF_S4UUserID_tags_1[0]), /* 1 */
	asn_DEF_S4UUserID_tags_1,	/* Same as above */
	sizeof(asn_DEF_S4UUserID_tags_1)
		/sizeof(asn_DEF_S4UUserID_tags_1[0]), /* 1 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_S4UUserID_1,
	5,	/* Elements count */
	&asn_SPC_S4UUserID_specs_1	/* Additional specs */
};

