/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#include "PA-DATA.h"

asn_TYPE_member_t asn_MBR_PA_DATA_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct PA_DATA, padata_type),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_PADATA_TYPE,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"padata-type"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct PA_DATA, padata_value),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_OCTET_STRING,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"padata-value"
		},
};
static const ber_tlv_tag_t asn_DEF_PA_DATA_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_PA_DATA_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 0, 0, 0 }, /* padata-type */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 1, 0, 0 } /* padata-value */
};
asn_SEQUENCE_specifics_t asn_SPC_PA_DATA_specs_1 = {
	sizeof(struct PA_DATA),
	offsetof(struct PA_DATA, _asn_ctx),
	asn_MAP_PA_DATA_tag2el_1,
	2,	/* Count of tags in the map */
	0, 0, 0,	/* Optional elements (not needed) */
	-1,	/* First extension addition */
};
asn_TYPE_descriptor_t asn_DEF_PA_DATA = {
	"PA-DATA",
	"PA-DATA",
	&asn_OP_SEQUENCE,
	asn_DEF_PA_DATA_tags_1,
	sizeof(asn_DEF_PA_DATA_tags_1)
		/sizeof(asn_DEF_PA_DATA_tags_1[0]), /* 1 */
	asn_DEF_PA_DATA_tags_1,	/* Same as above */
	sizeof(asn_DEF_PA_DATA_tags_1)
		/sizeof(asn_DEF_PA_DATA_tags_1[0]), /* 1 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_PA_DATA_1,
	2,	/* Elements count */
	&asn_SPC_PA_DATA_specs_1	/* Additional specs */
};

