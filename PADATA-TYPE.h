/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_PADATA_TYPE_H_
#define	_PADATA_TYPE_H_


#include <asn_application.h>

/* Including external dependencies */
#include <NativeInteger.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Dependencies */
typedef enum PADATA_TYPE {
	PADATA_TYPE_kRB5_PADATA_NONE	= 0,
	PADATA_TYPE_kRB5_PADATA_TGS_REQ	= 1,
	PADATA_TYPE_kRB5_PADATA_ENC_TIMESTAMP	= 2,
	PADATA_TYPE_kRB5_PADATA_PW_SALT	= 3,
	PADATA_TYPE_kRB5_PADATA_ENC_UNIX_TIME	= 5,
	PADATA_TYPE_kRB5_PADATA_SANDIA_SECUREID	= 6,
	PADATA_TYPE_kRB5_PADATA_SESAME	= 7,
	PADATA_TYPE_kRB5_PADATA_OSF_DCE	= 8,
	PADATA_TYPE_kRB5_PADATA_CYBERSAFE_SECUREID	= 9,
	PADATA_TYPE_kRB5_PADATA_AFS3_SALT	= 10,
	PADATA_TYPE_kRB5_PADATA_ETYPE_INFO	= 11,
	PADATA_TYPE_kRB5_PADATA_SAM_CHALLENGE	= 12,
	PADATA_TYPE_kRB5_PADATA_SAM_RESPONSE	= 13,
	PADATA_TYPE_kRB5_PADATA_PK_AS_REQ_19	= 14,
	PADATA_TYPE_kRB5_PADATA_PK_AS_REP_19	= 15,
	PADATA_TYPE_kRB5_PADATA_PK_AS_REQ	= 16,
	PADATA_TYPE_kRB5_PADATA_PK_AS_REP	= 17,
	PADATA_TYPE_kRB5_PADATA_PA_PK_OCSP_RESPONSE	= 18,
	PADATA_TYPE_kRB5_PADATA_ETYPE_INFO2	= 19,
	PADATA_TYPE_kRB5_PADATA_USE_SPECIFIED_KVNO	= 20,
	PADATA_TYPE_kRB5_PADATA_SAM_REDIRECT	= 21,
	PADATA_TYPE_kRB5_PADATA_GET_FROM_TYPED_DATA	= 22,
	PADATA_TYPE_kRB5_PADATA_SAM_ETYPE_INFO	= 23,
	PADATA_TYPE_kRB5_PADATA_SERVER_REFERRAL	= 25,
	PADATA_TYPE_kRB5_PADATA_ALT_PRINC	= 24,
	PADATA_TYPE_kRB5_PADATA_SAM_CHALLENGE2	= 30,
	PADATA_TYPE_kRB5_PADATA_SAM_RESPONSE2	= 31,
	PADATA_TYPE_kRB5_PA_EXTRA_TGT	= 41,
	PADATA_TYPE_kRB5_PADATA_FX_FAST_ARMOR	= 71,
	PADATA_TYPE_kRB5_PADATA_TD_KRB_PRINCIPAL	= 102,
	PADATA_TYPE_kRB5_PADATA_PK_TD_TRUSTED_CERTIFIERS	= 104,
	PADATA_TYPE_kRB5_PADATA_PK_TD_CERTIFICATE_INDEX	= 105,
	PADATA_TYPE_kRB5_PADATA_TD_APP_DEFINED_ERROR	= 106,
	PADATA_TYPE_kRB5_PADATA_TD_REQ_NONCE	= 107,
	PADATA_TYPE_kRB5_PADATA_TD_REQ_SEQ	= 108,
	PADATA_TYPE_kRB5_PADATA_PA_PAC_REQUEST	= 128,
	PADATA_TYPE_kRB5_PADATA_FOR_USER	= 129,
	PADATA_TYPE_kRB5_PADATA_FOR_X509_USER	= 130,
	PADATA_TYPE_kRB5_PADATA_FOR_CHECK_DUPS	= 131,
	PADATA_TYPE_kRB5_PADATA_PK_AS_09_BINDING	= 132,
	PADATA_TYPE_kRB5_PADATA_FX_COOKIE	= 133,
	PADATA_TYPE_kRB5_PADATA_AUTHENTICATION_SET	= 134,
	PADATA_TYPE_kRB5_PADATA_AUTH_SET_SELECTED	= 135,
	PADATA_TYPE_kRB5_PADATA_FX_FAST	= 136,
	PADATA_TYPE_kRB5_PADATA_FX_ERROR	= 137,
	PADATA_TYPE_kRB5_PADATA_ENCRYPTED_CHALLENGE	= 138,
	PADATA_TYPE_kRB5_PADATA_OTP_CHALLENGE	= 141,
	PADATA_TYPE_kRB5_PADATA_OTP_REQUEST	= 142,
	PADATA_TYPE_kBB5_PADATA_OTP_CONFIRM	= 143,
	PADATA_TYPE_kRB5_PADATA_OTP_PIN_CHANGE	= 144,
	PADATA_TYPE_kRB5_PADATA_EPAK_AS_REQ	= 145,
	PADATA_TYPE_kRB5_PADATA_EPAK_AS_REP	= 146,
	PADATA_TYPE_kRB5_PADATA_PKINIT_KX	= 147,
	PADATA_TYPE_kRB5_PADATA_PKU2U_NAME	= 148,
	PADATA_TYPE_kRB5_PADATA_REQ_ENC_PA_REP	= 149,
	PADATA_TYPE_kRB5_PADATA_SUPPORTED_ETYPES	= 165,
	PADATA_TYPE_kRB5_PADATA_PAC_OPTIONS	= 167
} e_PADATA_TYPE;

/* PADATA-TYPE */
typedef long	 PADATA_TYPE_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_PADATA_TYPE;
asn_struct_free_f PADATA_TYPE_free;
asn_struct_print_f PADATA_TYPE_print;
asn_constr_check_f PADATA_TYPE_constraint;
ber_type_decoder_f PADATA_TYPE_decode_ber;
der_type_encoder_f PADATA_TYPE_encode_der;
xer_type_decoder_f PADATA_TYPE_decode_xer;
xer_type_encoder_f PADATA_TYPE_encode_xer;
oer_type_decoder_f PADATA_TYPE_decode_oer;
oer_type_encoder_f PADATA_TYPE_encode_oer;
per_type_decoder_f PADATA_TYPE_decode_uper;
per_type_encoder_f PADATA_TYPE_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _PADATA_TYPE_H_ */
#include <asn_internal.h>
