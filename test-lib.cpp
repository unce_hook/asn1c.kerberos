#include "test-lib.h"
       int opt_debug;   /* -d (or -dd) */
static int opt_check;   /* -c (constraints checking) */
static int opt_stack;   /* -s (maximum stack size) */
static int opt_nopad;   /* -per-nopad (PER input is not padded between msgs) */
static int opt_onepdu;  /* -1 (decode single PDU) */

/* Dump the buffer out to the specified FILE */
static int write_out(const void *buffer, size_t size, void *key) {
    FILE *fp = (FILE *)key;
    return (fwrite(buffer, 1, size, fp) == size) ? 0 : -1;
}

int
main(int argc, char *argv[]) {
    Applications *app = 0;
    asn_dec_rval_t er;
    FILE *binary_out = stdout;
    setvbuf(stdout, 0, _IOLBF, 0);
    DataSize_t *dd = NULL;
    DataSize_t *head = dd;
    char erbuf[128];
    size_t erlen = sizeof(erbuf);

    if (argc > 1) {
        std::cout << "argv[1] = " << argv[1] << std::endl; 
    } else {
        std::cout << "No file name entered. Exiting...";
        return -1;
    }
    std::ifstream infile(argv[1], std::ifstream::binary); //open the file
    
    if (infile.is_open() && infile.good()) {
        infile.seekg (0, infile.end);
        int length = infile.tellg();
        infile.seekg (0, infile.beg);

        char * buffer = new char [length];

        std::cout << "Reading " << length << " characters... " << std::endl;
        // read data as a block:
        infile.read (buffer,length);
        int ret = asn_check_constraints(&asn_DEF_Applications, buffer, erbuf, &erlen);

        er = asn_decode(0, ATS_BER, &asn_DEF_Applications, (void **)&app, buffer, length, &dd);
        int len = list_length(dd);
        int *array_list = toArray(dd, len, 0);
        for (int i = 0; i < len; i++) {
            std::cout << array_list[i] << " ";
        }
        std::cout << std::endl;
        int *array_list1 = toArray(dd, len, 1);
        for (int i = 0; i < len; i++) {
            std::cout << array_list1[i] << " ";
        }
        int offset = 0;
        int size = 0;

        // printLinkedList(dd);
        ssize_t tag_len;
        ssize_t len_len;
        ber_tlv_tag_t tvl_tag;
        ber_tlv_len_t tlv_len;

        switch (app->present){
            case Applications_PR_ticket:
                std::cout << "Ticket_t" << std::endl;

                break;
            case Applications_PR_authenticator:
                std::cout << "Authenticator_t" << std::endl;
                break;
            case Applications_PR_encTicketPart:
                std::cout << "encTicketPart" << std::endl;
                break;
            case Applications_PR_as_req: {
                std::cout << "as_req" << std::endl;
                asn_encode(0, ATS_BASIC_XER, &asn_DEF_Applications, (void*)app, write_out, binary_out, &dd);
            }
                break;
            case Applications_PR_as_rep: {
                    // int offset = 0;
                std::cout << "as_rep "  << std::endl;
                asn_encode(0, ATS_BASIC_XER, &asn_DEF_Applications, (void*)app, write_out, binary_out, &dd);
                }
                break;
            case Applications_PR_tgs_req:
                std::cout << "tgs_req" << std::endl;
                break;
            case Applications_PR_tgs_rep: {
                    // int offset = 0;
                std::cout << "tgs_rep "  << std::endl;
                asn_encode(0, ATS_BASIC_XER, &asn_DEF_Applications, (void*)app, write_out, binary_out, &dd);
                }
                break;
            case Applications_PR_ap_req:
                std::cout << "ap_req" << std::endl;
                break;
            case Applications_PR_ap_rep:
                std::cout << "ap_rep" << std::endl;
                break;
            case Applications_PR_krb_safe:
                std::cout << "krb_safe" << std::endl;
                break;
            case Applications_PR_krb_priv:
                std::cout << "krb_priv" << std::endl;
                break;
            case Applications_PR_krb_cred:
                std::cout << "krb_cred" << std::endl;
                break;
            case Applications_PR_encASRepPart:
                std::cout << "encASRepPart" << std::endl;
                break;
            case Applications_PR_encTGSRepPart:
                std::cout << "encTGSRepPart" << std::endl;
                break;
            case Applications_PR_encAPRepPart:
                std::cout << "encAPRepPart" << std::endl;
                break;
            case Applications_PR_encKrbPrivPart:
                std::cout << "encKrbPrivPart" << std::endl;
                break;
            case Applications_PR_encKrbCredPart:
                std::cout << "encKrbCredPart" << std::endl;
                break;
            case Applications_PR_krb_error: {
                // int offset = 0;
                std::cout << "krb_error "  << std::endl;
                asn_encode(0, ATS_BASIC_XER, &asn_DEF_Applications, (void*)app, write_out, binary_out, &dd);
                }
                break;
            default:
                std::cout << "nothing" << std::endl;
                break;
        }

        // xer_fprint(stdout, &asn_DEF_Applications, app);
        //asn_fprint(stdout, &asn_DEF_Applications, app);


        infile.close();
        delete[] buffer;
        
    } else {
        std::cout << "Failed to open file..";
    }
    return 0;
}