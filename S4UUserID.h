/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_S4UUserID_H_
#define	_S4UUserID_H_


#include <asn_application.h>

/* Including external dependencies */
#include "Krb5uint32.h"
#include "Realm.h"
#include <OCTET_STRING.h>
#include <BIT_STRING.h>
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
struct PrincipalName;

/* S4UUserID */
typedef struct S4UUserID {
	Krb5uint32_t	 nonce;
	struct PrincipalName	*cname	/* OPTIONAL */;
	Realm_t	 crealm;
	OCTET_STRING_t	*subject_certificate	/* OPTIONAL */;
	BIT_STRING_t	*options	/* OPTIONAL */;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} S4UUserID_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_S4UUserID;
extern asn_SEQUENCE_specifics_t asn_SPC_S4UUserID_specs_1;
extern asn_TYPE_member_t asn_MBR_S4UUserID_1[5];

#ifdef __cplusplus
}
#endif

/* Referred external types */
#include "PrincipalName.h"

#endif	/* _S4UUserID_H_ */
#include <asn_internal.h>
