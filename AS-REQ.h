/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_AS_REQ_H_
#define	_AS_REQ_H_


#include <asn_application.h>

/* Including external dependencies */
#include "KDC-REQ.h"

#ifdef __cplusplus
extern "C" {
#endif

/* AS-REQ */
typedef KDC_REQ_t	 AS_REQ_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_AS_REQ;
asn_struct_free_f AS_REQ_free;
asn_struct_print_f AS_REQ_print;
asn_constr_check_f AS_REQ_constraint;
ber_type_decoder_f AS_REQ_decode_ber;
der_type_encoder_f AS_REQ_encode_der;
xer_type_decoder_f AS_REQ_decode_xer;
xer_type_encoder_f AS_REQ_encode_xer;
oer_type_decoder_f AS_REQ_decode_oer;
oer_type_encoder_f AS_REQ_encode_oer;
per_type_decoder_f AS_REQ_decode_uper;
per_type_encoder_f AS_REQ_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _AS_REQ_H_ */
#include <asn_internal.h>
