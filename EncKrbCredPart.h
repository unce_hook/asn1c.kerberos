/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_EncKrbCredPart_H_
#define	_EncKrbCredPart_H_


#include <asn_application.h>

/* Including external dependencies */
#include "UInt32.h"
#include "KerberosTime.h"
#include "Microseconds.h"
#include <asn_SEQUENCE_OF.h>
#include <constr_SEQUENCE_OF.h>
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
struct HostAddress;
struct KrbCredInfo;

/* EncKrbCredPart */
typedef struct EncKrbCredPart {
	struct EncKrbCredPart__ticket_info {
		A_SEQUENCE_OF(struct KrbCredInfo) list;
		
		/* Context for parsing across buffer boundaries */
		asn_struct_ctx_t _asn_ctx;
	} ticket_info;
	UInt32_t	*nonce	/* OPTIONAL */;
	KerberosTime_t	*timestamp	/* OPTIONAL */;
	Microseconds_t	*usec	/* OPTIONAL */;
	struct HostAddress	*s_address	/* OPTIONAL */;
	struct HostAddress	*r_address	/* OPTIONAL */;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} EncKrbCredPart_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_EncKrbCredPart;
extern asn_SEQUENCE_specifics_t asn_SPC_EncKrbCredPart_specs_1;
extern asn_TYPE_member_t asn_MBR_EncKrbCredPart_1[6];

#ifdef __cplusplus
}
#endif

/* Referred external types */
#include "HostAddress.h"
#include "KrbCredInfo.h"

#endif	/* _EncKrbCredPart_H_ */
#include <asn_internal.h>
