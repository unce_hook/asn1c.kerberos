/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#include "PrincipalName.h"

static asn_TYPE_member_t asn_MBR_name_string_3[] = {
	{ ATF_POINTER, 0, 0,
		(ASN_TAG_CLASS_UNIVERSAL | (27 << 2)),
		0,
		&asn_DEF_GeneralString,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		""
		},
};
static const ber_tlv_tag_t asn_DEF_name_string_tags_3[] = {
	(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static asn_SET_OF_specifics_t asn_SPC_name_string_specs_3 = {
	sizeof(struct PrincipalName__name_string),
	offsetof(struct PrincipalName__name_string, _asn_ctx),
	0,	/* XER encoding is XMLDelimitedItemList */
};
static /* Use -fall-defs-global to expose */
asn_TYPE_descriptor_t asn_DEF_name_string_3 = {
	"name-string",
	"name-string",
	&asn_OP_SEQUENCE_OF,
	asn_DEF_name_string_tags_3,
	sizeof(asn_DEF_name_string_tags_3)
		/sizeof(asn_DEF_name_string_tags_3[0]), /* 2 */
	asn_DEF_name_string_tags_3,	/* Same as above */
	sizeof(asn_DEF_name_string_tags_3)
		/sizeof(asn_DEF_name_string_tags_3[0]), /* 2 */
	{ 0, 0, SEQUENCE_OF_constraint },
	asn_MBR_name_string_3,
	1,	/* Single element */
	&asn_SPC_name_string_specs_3	/* Additional specs */
};

asn_TYPE_member_t asn_MBR_PrincipalName_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct PrincipalName, name_type),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_NAME_TYPE,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"name-type"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct PrincipalName, name_string),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		0,
		&asn_DEF_name_string_3,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"name-string"
		},
};
static const ber_tlv_tag_t asn_DEF_PrincipalName_tags_1[] = {
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_PrincipalName_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* name-type */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 } /* name-string */
};
asn_SEQUENCE_specifics_t asn_SPC_PrincipalName_specs_1 = {
	sizeof(struct PrincipalName),
	offsetof(struct PrincipalName, _asn_ctx),
	asn_MAP_PrincipalName_tag2el_1,
	2,	/* Count of tags in the map */
	0, 0, 0,	/* Optional elements (not needed) */
	-1,	/* First extension addition */
};
asn_TYPE_descriptor_t asn_DEF_PrincipalName = {
	"PrincipalName",
	"PrincipalName",
	&asn_OP_SEQUENCE,
	asn_DEF_PrincipalName_tags_1,
	sizeof(asn_DEF_PrincipalName_tags_1)
		/sizeof(asn_DEF_PrincipalName_tags_1[0]), /* 1 */
	asn_DEF_PrincipalName_tags_1,	/* Same as above */
	sizeof(asn_DEF_PrincipalName_tags_1)
		/sizeof(asn_DEF_PrincipalName_tags_1[0]), /* 1 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_PrincipalName_1,
	2,	/* Elements count */
	&asn_SPC_PrincipalName_specs_1	/* Additional specs */
};

