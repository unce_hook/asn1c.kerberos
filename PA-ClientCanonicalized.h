/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_PA_ClientCanonicalized_H_
#define	_PA_ClientCanonicalized_H_


#include <asn_application.h>

/* Including external dependencies */
#include "PA-ClientCanonicalizedNames.h"
#include "Checksum.h"
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* PA-ClientCanonicalized */
typedef struct PA_ClientCanonicalized {
	PA_ClientCanonicalizedNames_t	 names;
	Checksum_t	 canon_checksum;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} PA_ClientCanonicalized_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_PA_ClientCanonicalized;

#ifdef __cplusplus
}
#endif

#endif	/* _PA_ClientCanonicalized_H_ */
#include <asn_internal.h>
