/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_EtypeList_H_
#define	_EtypeList_H_


#include <asn_application.h>

/* Including external dependencies */
#include "Krb5int32.h"
#include <asn_SEQUENCE_OF.h>
#include <constr_SEQUENCE_OF.h>

#ifdef __cplusplus
extern "C" {
#endif

/* EtypeList */
typedef struct EtypeList {
	A_SEQUENCE_OF(Krb5int32_t) list;
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} EtypeList_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_EtypeList;

#ifdef __cplusplus
}
#endif

#endif	/* _EtypeList_H_ */
#include <asn_internal.h>
