/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_PA_SAM_CHALLENGE_2_H_
#define	_PA_SAM_CHALLENGE_2_H_


#include <asn_application.h>

/* Including external dependencies */
#include "PA-SAM-CHALLENGE-2-BODY.h"
#include <asn_SEQUENCE_OF.h>
#include <constr_SEQUENCE_OF.h>
#include <constr_SEQUENCE.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Forward declarations */
struct Checksum;

/* PA-SAM-CHALLENGE-2 */
typedef struct PA_SAM_CHALLENGE_2 {
	PA_SAM_CHALLENGE_2_BODY_t	 sam_body;
	struct PA_SAM_CHALLENGE_2__sam_cksum {
		A_SEQUENCE_OF(struct Checksum) list;
		
		/* Context for parsing across buffer boundaries */
		asn_struct_ctx_t _asn_ctx;
	} sam_cksum;
	/*
	 * This type is extensible,
	 * possible extensions are below.
	 */
	
	/* Context for parsing across buffer boundaries */
	asn_struct_ctx_t _asn_ctx;
} PA_SAM_CHALLENGE_2_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_PA_SAM_CHALLENGE_2;

#ifdef __cplusplus
}
#endif

/* Referred external types */
#include "Checksum.h"

#endif	/* _PA_SAM_CHALLENGE_2_H_ */
#include <asn_internal.h>
