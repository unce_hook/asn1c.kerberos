/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#include "KRB-ERROR.h"

static int
memb_pvno_constraint_1(const asn_TYPE_descriptor_t *td, const void *sptr,
			asn_app_constraint_failed_f *ctfailcb, void *app_key) {
	long value;
	
	if(!sptr) {
		ASN__CTFAIL(app_key, td, sptr,
			"%s: value not given (%s:%d)",
			td->name, __FILE__, __LINE__);
		return -1;
	}
	
	value = *(const long *)sptr;
	
	if((value == 5)) {
		/* Constraint check succeeded */
		return 0;
	} else {
		ASN__CTFAIL(app_key, td, sptr,
			"%s: constraint failed (%s:%d)",
			td->name, __FILE__, __LINE__);
		return -1;
	}
}

static asn_oer_constraints_t asn_OER_memb_pvno_constr_2 CC_NOTUSED = {
	{ 1, 1 }	/* (5..5) */,
	-1};
static asn_per_constraints_t asn_PER_memb_pvno_constr_2 CC_NOTUSED = {
	{ APC_CONSTRAINED,	 0,  0,  5,  5 }	/* (5..5) */,
	{ APC_UNCONSTRAINED,	-1, -1,  0,  0 },
	0, 0	/* No PER value map */
};
asn_TYPE_member_t asn_MBR_KRB_ERROR_1[] = {
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, pvno),
		(ASN_TAG_CLASS_CONTEXT | (0 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_NativeInteger,
		0,
		{ &asn_OER_memb_pvno_constr_2, &asn_PER_memb_pvno_constr_2,  memb_pvno_constraint_1 },
		0, 0, /* No default value */
		"pvno"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, msg_type),
		(ASN_TAG_CLASS_CONTEXT | (1 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_MESSAGE_TYPE,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"msg-type"
		},
	{ ATF_POINTER, 2, offsetof(struct KRB_ERROR, ctime),
		(ASN_TAG_CLASS_CONTEXT | (2 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_KerberosTime,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"ctime"
		},
	{ ATF_POINTER, 1, offsetof(struct KRB_ERROR, cusec),
		(ASN_TAG_CLASS_CONTEXT | (3 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Microseconds,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"cusec"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, stime),
		(ASN_TAG_CLASS_CONTEXT | (4 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_KerberosTime,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"stime"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, susec),
		(ASN_TAG_CLASS_CONTEXT | (5 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Microseconds,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"susec"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, error_code),
		(ASN_TAG_CLASS_CONTEXT | (6 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_ERROR_CODE,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"error-code"
		},
	{ ATF_POINTER, 2, offsetof(struct KRB_ERROR, crealm),
		(ASN_TAG_CLASS_CONTEXT | (7 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Realm,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"crealm"
		},
	{ ATF_POINTER, 1, offsetof(struct KRB_ERROR, cname),
		(ASN_TAG_CLASS_CONTEXT | (8 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_CName,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"cname"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, realm),
		(ASN_TAG_CLASS_CONTEXT | (9 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Realm,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"realm"
		},
	{ ATF_NOFLAGS, 0, offsetof(struct KRB_ERROR, sname),
		(ASN_TAG_CLASS_CONTEXT | (10 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_SName,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"sname"
		},
	{ ATF_POINTER, 3, offsetof(struct KRB_ERROR, e_text),
		(ASN_TAG_CLASS_CONTEXT | (11 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_KerberosString,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"e-text"
		},
	{ ATF_POINTER, 2, offsetof(struct KRB_ERROR, e_data),
		(ASN_TAG_CLASS_CONTEXT | (12 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_OCTET_STRING,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"e-data"
		},
	{ ATF_POINTER, 1, offsetof(struct KRB_ERROR, e_checksum),
		(ASN_TAG_CLASS_CONTEXT | (13 << 2)),
		+1,	/* EXPLICIT tag at current level */
		&asn_DEF_Checksum,
		0,
		{ 0, 0, 0 },
		0, 0, /* No default value */
		"e-checksum"
		},
};
static const int asn_MAP_KRB_ERROR_oms_1[] = { 2, 3, 7, 8, 11, 12, 13 };
static const ber_tlv_tag_t asn_DEF_KRB_ERROR_tags_1[] = {
	(ASN_TAG_CLASS_APPLICATION | (30 << 2)),
	(ASN_TAG_CLASS_UNIVERSAL | (16 << 2))
};
static const asn_TYPE_tag2member_t asn_MAP_KRB_ERROR_tag2el_1[] = {
    { (ASN_TAG_CLASS_CONTEXT | (0 << 2)), 0, 0, 0 }, /* pvno */
    { (ASN_TAG_CLASS_CONTEXT | (1 << 2)), 1, 0, 0 }, /* msg-type */
    { (ASN_TAG_CLASS_CONTEXT | (2 << 2)), 2, 0, 0 }, /* ctime */
    { (ASN_TAG_CLASS_CONTEXT | (3 << 2)), 3, 0, 0 }, /* cusec */
    { (ASN_TAG_CLASS_CONTEXT | (4 << 2)), 4, 0, 0 }, /* stime */
    { (ASN_TAG_CLASS_CONTEXT | (5 << 2)), 5, 0, 0 }, /* susec */
    { (ASN_TAG_CLASS_CONTEXT | (6 << 2)), 6, 0, 0 }, /* error-code */
    { (ASN_TAG_CLASS_CONTEXT | (7 << 2)), 7, 0, 0 }, /* crealm */
    { (ASN_TAG_CLASS_CONTEXT | (8 << 2)), 8, 0, 0 }, /* cname */
    { (ASN_TAG_CLASS_CONTEXT | (9 << 2)), 9, 0, 0 }, /* realm */
    { (ASN_TAG_CLASS_CONTEXT | (10 << 2)), 10, 0, 0 }, /* sname */
    { (ASN_TAG_CLASS_CONTEXT | (11 << 2)), 11, 0, 0 }, /* e-text */
    { (ASN_TAG_CLASS_CONTEXT | (12 << 2)), 12, 0, 0 }, /* e-data */
    { (ASN_TAG_CLASS_CONTEXT | (13 << 2)), 13, 0, 0 } /* e-checksum */
};
asn_SEQUENCE_specifics_t asn_SPC_KRB_ERROR_specs_1 = {
	sizeof(struct KRB_ERROR),
	offsetof(struct KRB_ERROR, _asn_ctx),
	asn_MAP_KRB_ERROR_tag2el_1,
	14,	/* Count of tags in the map */
	asn_MAP_KRB_ERROR_oms_1,	/* Optional members */
	7, 0,	/* Root/Additions */
	-1,	/* First extension addition */
};
asn_TYPE_descriptor_t asn_DEF_KRB_ERROR = {
	"KRB-ERROR",
	"KRB-ERROR",
	&asn_OP_SEQUENCE,
	asn_DEF_KRB_ERROR_tags_1,
	sizeof(asn_DEF_KRB_ERROR_tags_1)
		/sizeof(asn_DEF_KRB_ERROR_tags_1[0]), /* 2 */
	asn_DEF_KRB_ERROR_tags_1,	/* Same as above */
	sizeof(asn_DEF_KRB_ERROR_tags_1)
		/sizeof(asn_DEF_KRB_ERROR_tags_1[0]), /* 2 */
	{ 0, 0, SEQUENCE_constraint },
	asn_MBR_KRB_ERROR_1,
	14,	/* Elements count */
	&asn_SPC_KRB_ERROR_specs_1	/* Additional specs */
};

