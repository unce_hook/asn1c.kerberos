/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_PROV_SRV_LOCATION_H_
#define	_PROV_SRV_LOCATION_H_


#include <asn_application.h>

/* Including external dependencies */
#include <GeneralString.h>

#ifdef __cplusplus
extern "C" {
#endif

/* PROV-SRV-LOCATION */
typedef GeneralString_t	 PROV_SRV_LOCATION_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_PROV_SRV_LOCATION;
asn_struct_free_f PROV_SRV_LOCATION_free;
asn_struct_print_f PROV_SRV_LOCATION_print;
asn_constr_check_f PROV_SRV_LOCATION_constraint;
ber_type_decoder_f PROV_SRV_LOCATION_decode_ber;
der_type_encoder_f PROV_SRV_LOCATION_encode_der;
xer_type_decoder_f PROV_SRV_LOCATION_decode_xer;
xer_type_encoder_f PROV_SRV_LOCATION_encode_xer;
oer_type_decoder_f PROV_SRV_LOCATION_decode_oer;
oer_type_encoder_f PROV_SRV_LOCATION_encode_oer;
per_type_decoder_f PROV_SRV_LOCATION_decode_uper;
per_type_encoder_f PROV_SRV_LOCATION_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _PROV_SRV_LOCATION_H_ */
#include <asn_internal.h>
