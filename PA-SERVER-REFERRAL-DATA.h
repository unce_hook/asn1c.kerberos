/*
 * Generated by asn1c-0.9.29 (http://lionet.info/asn1c)
 * From ASN.1 module "KERBEROS5"
 * 	found in "./kerberos.asn"
 * 	`asn1c -S ./asn1c/skeletons -pdu=Applications -pdu=auto -fcompound-names`
 */

#ifndef	_PA_SERVER_REFERRAL_DATA_H_
#define	_PA_SERVER_REFERRAL_DATA_H_


#include <asn_application.h>

/* Including external dependencies */
#include "EncryptedData.h"

#ifdef __cplusplus
extern "C" {
#endif

/* PA-SERVER-REFERRAL-DATA */
typedef EncryptedData_t	 PA_SERVER_REFERRAL_DATA_t;

/* Implementation */
extern asn_TYPE_descriptor_t asn_DEF_PA_SERVER_REFERRAL_DATA;
asn_struct_free_f PA_SERVER_REFERRAL_DATA_free;
asn_struct_print_f PA_SERVER_REFERRAL_DATA_print;
asn_constr_check_f PA_SERVER_REFERRAL_DATA_constraint;
ber_type_decoder_f PA_SERVER_REFERRAL_DATA_decode_ber;
der_type_encoder_f PA_SERVER_REFERRAL_DATA_encode_der;
xer_type_decoder_f PA_SERVER_REFERRAL_DATA_decode_xer;
xer_type_encoder_f PA_SERVER_REFERRAL_DATA_encode_xer;
oer_type_decoder_f PA_SERVER_REFERRAL_DATA_decode_oer;
oer_type_encoder_f PA_SERVER_REFERRAL_DATA_encode_oer;
per_type_decoder_f PA_SERVER_REFERRAL_DATA_decode_uper;
per_type_encoder_f PA_SERVER_REFERRAL_DATA_encode_uper;

#ifdef __cplusplus
}
#endif

#endif	/* _PA_SERVER_REFERRAL_DATA_H_ */
#include <asn_internal.h>
